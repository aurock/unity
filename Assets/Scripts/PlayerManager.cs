using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    private float horizontal;
    [SerializeField] private float speed = 9f;
    [SerializeField] private float jumpingPower = 9f;
    private Vector2 position = new Vector2(-4, 3);

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    public bool isLeftPressed;
    public bool isRighttPressed;

    void Update() {
        Move();
    }

    private void FixedUpdate() {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
        horizontal = 0;
    }

    private bool IsGrounded() {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    public void Move() {
        //horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump") && IsGrounded()) {
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);
        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f) {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        if (isLeftPressed) {
            horizontal = -1;
        } else if (isRighttPressed) {
            horizontal = 1;
        } else {
            horizontal = 0;
        }
    }

    public void MoveRight() {
        isRighttPressed = !isRighttPressed;
    }

    public void MoveLeft() {
        isLeftPressed = !isLeftPressed;
    }

    public void Jump() {
        Debug.Log("Jump");
        if (IsGrounded()) {
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);
        }
    }
}
