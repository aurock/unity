using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public PlayerManager player1, player2;
    public BallManager ball;
    public Transform spawnPlayer1, spawnPlayer2;
    public Transform ballPlayer1, ballPlayer2;
    public TMP_Text scoreTxt, timerTxt;

    private float tmpTimer;
    private bool StartingGame = false;
    private int[] score;

    void Awake() {
        BallManager.goal += ManagerScore;
        score = new int[2];
        InitGame(false);
    }

    private void OnDestroy() {
        BallManager.goal -= ManagerScore;
    }

    private void Update() {
        if (StartingGame) {
            StartingTimer();
        }
    }

    public void InitGame(bool leftOrRight = true) {
        ManagerRBBall(false);
        player1.transform.position = spawnPlayer1.position;
        player2.transform.position = spawnPlayer2.position;
        ball.transform.position = leftOrRight ?  ballPlayer2.position : ballPlayer1.position;
        StartingGame = true;
    }

    public void ManagerRBBall(bool simulated) {
        ball.rb.velocity = Vector2.zero;
        ball.rb.simulated = simulated;
    }

    //left : true, right : false
    public void ManagerScore(bool leftOrRight) {
        Debug.Log("ManagerScore");
        if (leftOrRight) {
            score[1]++;
        } else {
            score[0]++;
        }

        UpdateScore();
        InitGame(leftOrRight);
    }

    public void UpdateScore() {
        scoreTxt.text = score[0] + " - " + score[1];
    }

    public void StartingTimer() {
        tmpTimer += Time.deltaTime;
        if (tmpTimer >= 3.5f) {
            timerTxt.text = "";
            tmpTimer = 0;
            StartingGame = false;
            ManagerRBBall(true);
        }else if (tmpTimer >= 3) {
            timerTxt.text = "Play !";
        } else {
            timerTxt.text = (3 - (int) tmpTimer).ToString();
        }
    }
}
