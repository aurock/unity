using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour {
    public Rigidbody2D rb;
    public LayerMask layerMask;
    public static event Action<bool> goal;

    //left : true, right : false
    public void onGoal(bool leftOrRight) {
        goal?.Invoke(leftOrRight);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        Debug.Log("Collision ball");
        if ((layerMask.value & (1 << collision.gameObject.layer)) > 0) {
            onGoal(collision.gameObject.CompareTag("floor_left"));
        }
    }
}
