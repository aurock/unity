using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void StartPlaying() {
        SceneManager.LoadSceneAsync("Main", LoadSceneMode.Single);
    }

    public void CloseApp() {
        Application.Quit();
    }

    public void LoadMenu() {
        SceneManager.LoadSceneAsync("Menu", LoadSceneMode.Single);
    }
}
